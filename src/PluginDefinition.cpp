//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "PluginDefinition.h"
#include "menuCmdID.h"
#include <string>

//#pragma warning(disable : C2220)

//
// The plugin data that Notepad++ needs
//
FuncItem funcItem[nbFunc];

//
// The data of Notepad++ that you can use in your plugin commands
//
NppData nppData;

//
// Initialize your plugin data here
// It will be called while plugin loading   
void pluginInit(HANDLE /*hModule*/)
{
}

//
// Here you can do the clean up, save the parameters (if any) for the next session
//
void pluginCleanUp()
{
}

ShortcutKey* prettyPrintShortCut;

//
// Initialization of your plugin commands
// You should fill your plugins commands here
void commandMenuInit()
{

    //--------------------------------------------//
    //-- STEP 3. CUSTOMIZE YOUR PLUGIN COMMANDS --//
    //--------------------------------------------//
    // with function :
    // setCommand(int index,                      // zero based number to indicate the order of command
    //            TCHAR *commandName,             // the command name that you want to see in plugin menu
    //            PFUNCPLUGINCMD functionPointer, // the symbol of function (function pointer) associated with this command. The body should be defined below. See Step 4.
    //            ShortcutKey *shortcut,          // optional. Define a shortcut to trigger this command
    //            bool check0nInit                // optional. Make this menu item be checked visually
    //            );

	prettyPrintShortCut = new ShortcutKey;

	prettyPrintShortCut->_isAlt = false;
	prettyPrintShortCut->_isCtrl = true;
	prettyPrintShortCut->_isShift = true;
	prettyPrintShortCut->_key = 'A';


    setCommand(0, TEXT("Pretty Print HTML or XML"), hello, prettyPrintShortCut, false);
    setCommand(1, TEXT("About"), helloDlg, NULL, false);
}

//
// Here you can do the clean up (especially for the shortcut)
//
void commandMenuCleanUp()
{
	// Don't forget to deallocate your shortcut here
	delete prettyPrintShortCut;
}


//
// This function help you to initialize your plugin commands
//
bool setCommand(size_t index, TCHAR *cmdName, PFUNCPLUGINCMD pFunc, ShortcutKey *sk, bool check0nInit) 
{
    if (index >= nbFunc)
        return false;

    if (!pFunc)
        return false;

    lstrcpy(funcItem[index]._itemName, cmdName);
    funcItem[index]._pFunc = pFunc;
    funcItem[index]._init2Check = check0nInit;
    funcItem[index]._pShKey = sk;

    return true;
}

void prettyPrint(bool a, bool b);

int nbopenfiles1;
int nbopenfiles2;

//----------------------------------------------//
//-- STEP 4. DEFINE YOUR ASSOCIATED FUNCTIONS --//
//----------------------------------------------//
void hello()
{
    //// Open a new document
    //::SendMessage(nppData._nppHandle, NPPM_MENUCOMMAND, 0, IDM_FILE_NEW);

    //// Get the current scintilla
    //int which = -1;
    //::SendMessage(nppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&which);
    //if (which == -1)
    //    return;
    //HWND curScintilla = (which == 0)?nppData._scintillaMainHandle:nppData._scintillaSecondHandle;

    //// Say hello now :
    //// Scintilla control has no Unicode mode, so we use (char *) here
    //::SendMessage(curScintilla, SCI_SETTEXT, 0, (LPARAM)"Hello, Notepad++!");

	//invoke the prettyPrint function - pretty much taken verbatim from the XML Tools source
	prettyPrint(true, true);
}

void helloDlg()
{
    ::MessageBox(NULL, TEXT("Pretty Print your HTML or XML files with ease. Uses code from the XML Tools plugin for Notepad++ to perform the pretty printing of a file."), TEXT("Simple Pretty Print for HTML/XML!"), MB_OK);
}

bool hasNextDoc(int* iter) {
	//dbgln("hasNextDoc()");

	if (false) {
	//if (doPrettyPrintAllOpenFiles) {
		if (*iter < 0 || *iter >= (nbopenfiles1 + nbopenfiles2)) return false;

		if (*iter < nbopenfiles1) {
			::SendMessage(nppData._nppHandle, NPPM_ACTIVATEDOC, MAIN_VIEW, (*iter));
		}
		else if (*iter >= nbopenfiles1 && *iter < nbopenfiles1 + nbopenfiles2) {
			::SendMessage(nppData._nppHandle, NPPM_ACTIVATEDOC, SUB_VIEW, (*iter) - nbopenfiles1);
		}
		else {
			return false;
		}

		++(*iter);
		return true;
	}
	else {
		++(*iter);
		return ((*iter) == 1);
	}
}

int initDocIterator() {
	//dbgln("initDocIterator()");

	nbopenfiles1 = (int) ::SendMessage(nppData._nppHandle, NPPM_GETNBOPENFILES, 0, PRIMARY_VIEW);
	nbopenfiles2 = (int) ::SendMessage(nppData._nppHandle, NPPM_GETNBOPENFILES, 0, SECOND_VIEW);

	if (::SendMessage(nppData._nppHandle, NPPM_GETCURRENTDOCINDEX, 0, MAIN_VIEW) < 0) nbopenfiles1 = 0;
	if (::SendMessage(nppData._nppHandle, NPPM_GETCURRENTDOCINDEX, 0, SUB_VIEW) < 0) nbopenfiles2 = 0;

	//Report::_printf_inf(Report::str_format("%d %d",nbopenfiles1,nbopenfiles2));

	return 0;
}

HWND getCurrentHScintilla(int which) {
	return (which == 0) ? nppData._scintillaMainHandle : nppData._scintillaSecondHandle;
};

void getEOLChar(HWND hwnd, int* eolmode, std::string* eolchar) {
	*eolmode = ::SendMessage(hwnd, SCI_GETEOLMODE, 0, 0);
	switch (*eolmode) {
	case SC_EOL_CR:
		*eolchar = "\r";
		break;
	case SC_EOL_LF:
		*eolchar = "\n";
		break;
	case SC_EOL_CRLF:
	default:
		*eolchar = "\r\n";
	}
}

bool isEOL(const std::string& txt, const std::string::size_type txtlength, unsigned int pos, int mode) {
	switch (mode) {
	case SC_EOL_CR:
		return (txt.at(pos) == '\r');
		break;
	case SC_EOL_LF:
		return (txt.at(pos) == '\n');
		break;
	case SC_EOL_CRLF:
	default:
		return (txtlength > pos && txt.at(pos) == '\r' && txt.at(pos + 1) == '\n');
		break;
	}
}

bool isEOL(const char cc, const char nc, int mode) {
	switch (mode) {
	case SC_EOL_CR:
		return (cc == '\r');
		break;
	case SC_EOL_LF:
		return (cc == '\n');
		break;
	case SC_EOL_CRLF:
	default:
		return (cc == '\r' && nc == '\n');
		break;
	}
}

//both params are always true
void prettyPrint(bool autoindenttext, bool addlinebreaks) {
	//dbgln("prettyPrint()");

	int docIterator = initDocIterator();
	while (hasNextDoc(&docIterator)) {
		int currentEdit, currentLength, isReadOnly, xOffset, yOffset;
		::SendMessage(nppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&currentEdit);
		HWND hCurrentEditView = getCurrentHScintilla(currentEdit);

		isReadOnly = (int) ::SendMessage(hCurrentEditView, SCI_GETREADONLY, 0, 0);
		if (isReadOnly) return;

		xOffset = (int) ::SendMessage(hCurrentEditView, SCI_GETXOFFSET, 0, 0);
		yOffset = (int) ::SendMessage(hCurrentEditView, SCI_GETFIRSTVISIBLELINE, 0, 0);

		char *data = NULL;

		// count the < and > signs; > are ignored if tagsignlevel <= 0. This prevent indent errors if text or attributes contain > sign.
		int tagsignlevel = 0;
		// some state variables
		bool in_comment = false, in_header = false, in_attribute = false, in_nodetext = false, in_cdata = false, in_text = false;

		// some counters
		std::string::size_type curpos = 0, strlength = 0, prevspecchar, nexwchar_t, deltapos, tagend, startprev, endnext;
		long xmllevel = 0;
		// some char value (pc = previous char, cc = current char, nc = next char, nnc = next next char)
		char pc, cc, nc, nnc;

		int tabwidth = ::SendMessage(hCurrentEditView, SCI_GETTABWIDTH, 0, 0);
		int usetabs = ::SendMessage(hCurrentEditView, SCI_GETUSETABS, 0, 0);
		if (tabwidth <= 0) tabwidth = 4;

		bool isclosingtag;

		// use the selection
		long selstart = 0, selend = 0;

		selstart = ::SendMessage(hCurrentEditView, SCI_GETSELECTIONSTART, 0, 0);
		selend = ::SendMessage(hCurrentEditView, SCI_GETSELECTIONEND, 0, 0);

		std::string str("");
		std::string eolchar;
		int eolmode;
		getEOLChar(hCurrentEditView, &eolmode, &eolchar);

		if (selend > selstart) {
			currentLength = selend - selstart;
			data = new char[currentLength + 1];
			if (!data) return;  // allocation error, abort check
			memset(data, '\0', currentLength + 1);
			::SendMessage(hCurrentEditView, SCI_GETSELTEXT, 0, reinterpret_cast<LPARAM>(data));
		}
		else {
			currentLength = (int) ::SendMessage(hCurrentEditView, SCI_GETLENGTH, 0, 0);
			data = new char[currentLength + 1];
			if (!data) return;  // allocation error, abort check
			memset(data, '\0', currentLength + 1);
			::SendMessage(hCurrentEditView, SCI_GETTEXT, currentLength + 1, reinterpret_cast<LPARAM>(data));
		}



		str += data;
		delete[] data;
		data = NULL;

		// Proceed to first pass if break adds are enabled
		if (addlinebreaks) {
			while (curpos < str.length() && (curpos = str.find_first_of("<>", curpos)) != std::string::npos) {
				cc = str.at(curpos);

				if (cc == '<' && curpos < str.length() - 4 && !str.compare(curpos, 4, "<!--")) {
					// Let's skip the comment
					curpos = str.find("-->", curpos + 1) + 2;
					// Adds a line break after comment if required
					nexwchar_t = str.find_first_not_of(" \t", curpos + 1);
					if (nexwchar_t != std::string::npos && str.at(nexwchar_t) == '<') {
						str.insert(++curpos, eolchar);
					}
				}
				else if (cc == '<' && curpos < str.length() - 9 && !str.compare(curpos, 9, "<![CDATA[")) {
					// Let's skip the CDATA block
					curpos = str.find("]]>", curpos + 1) + 2;
					// Adds a line break after CDATA if required
					nexwchar_t = str.find_first_not_of(" \t", curpos + 1);
					if (nexwchar_t != std::string::npos && str.at(nexwchar_t) == '<') {
						str.insert(++curpos, eolchar);
					}
				}
				else if (cc == '<' && curpos < str.length() - 5 && !str.compare(curpos, 5, "<meta")) {
					// Let's skip the meta block
					curpos = str.find(">", curpos + 1);// +2;
					// Adds a line break after meta if required
					nexwchar_t = str.find_first_not_of(" \t", curpos + 1);
					if (nexwchar_t != std::string::npos && str.at(nexwchar_t) == '<') {
						str.insert(++curpos, eolchar);
					}
				}
				else if (cc == '<' && curpos < str.length() - 5 && !str.compare(curpos, 5, "<link")) {
					// Let's skip the link block
					curpos = str.find(">", curpos + 1);// +2;
													   // Adds a line break after meta if required
					nexwchar_t = str.find_first_not_of(" \t", curpos + 1);
					if (nexwchar_t != std::string::npos && str.at(nexwchar_t) == '<') {
						str.insert(++curpos, eolchar);
					}
				}
				else if (cc == '>') {
					// Let's see if '>' is a end tag char (it might also be simple text)
					// To perform test, we search last of "<>". If it is a '>', current '>' is
					// simple text, if not, it is a end tag char. '>' text chars are ignored.
					prevspecchar = str.find_last_of("<>", curpos - 1);
					if (prevspecchar != std::string::npos) {
						// let's see if our '>' is in attribute
						std::string::size_type nextt = str.find_first_of("\"'", prevspecchar + 1);
						if (str.at(prevspecchar) == '>' && (nextt == std::string::npos || nextt > curpos)) {
							// current > is simple text, in text node
							++curpos;
							continue;
						}
						nextt = str.find_first_of("<>", curpos + 1);
						if (nextt != std::string::npos && str.at(nextt) == '>') {
							// current > is text in attribute
							++curpos;
							continue;
						}

						// We have found a '>' char and we are in a tag, let's see if it's an opening or closing one
						isclosingtag = ((curpos > 0 && str.at(curpos - 1) == '/') || str.at(prevspecchar + 1) == '/');

						nexwchar_t = str.find_first_not_of(" \t", curpos + 1);
						deltapos = nexwchar_t - curpos;
						// Test below identifies a <x><y> case and excludes <x>abc<y> case; in second case, we won't add line break
						if (nexwchar_t != std::string::npos && str.at(nexwchar_t) == '<' && curpos < str.length() - (deltapos + 1)) {
							// we compare previous and next tags; if they are same, we don't add line break
							startprev = str.rfind("<", curpos);
							endnext = str.find(">", nexwchar_t);

							if (startprev != std::string::npos && endnext != std::string::npos && curpos > startprev && endnext > nexwchar_t) {
								tagend = str.find_first_of(" />", startprev + 1);
								std::string tag1(str.substr(startprev + 1, tagend - startprev - 1));
								tagend = str.find_first_of(" />", nexwchar_t + 2);
								std::string tag2(str.substr(nexwchar_t + 2, tagend - nexwchar_t - 2));
								if (strcmp(tag1.c_str(), tag2.c_str()) || isclosingtag) {
									// Case of "<data><data>..." -> add a line break between tags
									str.insert(++curpos, eolchar);
								}
								else if (str.at(nexwchar_t + 1) == '/' && !isclosingtag && nexwchar_t == curpos + 1) {
									// Case of "<data id="..."></data>" -> replace by "<data id="..."/>"
									str.replace(curpos, endnext - curpos, "/");
								}
							}
						}
					}
				}

				++curpos;           // go to next char
			}


			// reinitialize cursor pos for second pass
			curpos = 0;
		}

		// Proceed to reformatting (second pass)
		prevspecchar = std::string::npos;
		std::string sep("<>\"'");
		char attributeQuote = '\0';
		sep += eolchar;
		strlength = str.length();

		bool in_meta = false;
		bool in_link = false;

		while (curpos < strlength && (curpos = str.find_first_of(sep, curpos)) != std::string::npos) {
			if (!isEOL(str, strlength, curpos, eolmode)) {
				if (curpos < strlength - 4 && !str.compare(curpos, 4, "<!--")) {
					in_comment = true;
				}
				if (curpos < strlength - 9 && !str.compare(curpos, 9, "<![CDATA[")) {
					in_cdata = true;
				}
				else if (curpos < strlength - 2 && !str.compare(curpos, 2, "<?")) {
					in_header = true;
				}
				else if (curpos < strlength - 5 && !str.compare(curpos, 5, "<meta")) {
					in_meta = true;
				}
				else if (curpos < strlength - 5 && !str.compare(curpos, 5, "<link")) {
					in_link = true;
				}
				else if (!in_comment && !in_cdata && !in_header && !in_meta && !in_link &&
					curpos < strlength && (str.at(curpos) == '\"' || str.at(curpos) == '\'')) {
					if (in_attribute && attributeQuote != '\0' && str.at(curpos) == attributeQuote && prevspecchar != std::string::npos && str.at(prevspecchar) == '<') {
						// attribute end
						in_attribute = false;
						attributeQuote = '\0';  // store the attribute quote char to detect the end of attribute
					}
					else if (!in_attribute) {
						std::string::size_type x = str.find_last_not_of(" \t", curpos - 1);
						std::string::size_type tagbeg = str.find_last_of("<>", x - 1);
						std::string::size_type tagend = str.find_first_of("<>", curpos + 1);
						if (x != std::string::npos && tagbeg != std::string::npos && tagend != std::string::npos &&
							str.at(x) == '=' && str.at(tagbeg) == '<' && str.at(tagend) == '>') {
							in_attribute = true;
							attributeQuote = str.at(curpos);  // store the attribute quote char to detect the end of attribute
						}
					}
				}
			}

			if (!in_comment && !in_cdata && !in_header && !in_meta && !in_link) {
				if (curpos > 0) {
					pc = str.at(curpos - 1);
				}
				else {
					pc = ' ';
				}

				cc = str.at(curpos);

				if (curpos < strlength - 1) {
					nc = str.at(curpos + 1);
				}
				else {
					nc = ' ';
				}

				if (curpos < strlength - 2) {
					nnc = str.at(curpos + 2);
				}
				else {
					nnc = ' ';
				}

				// managing of case where '>' char is present in text content
				in_text = false;
				if (cc == '>') {
					std::string::size_type tmppos = str.find_last_of("<>\"'", curpos - 1);
					// skip attributes which can contain > char
					while (tmppos != std::string::npos && (str.at(tmppos) == '\"' || str.at(tmppos) == '\'')) {
						tmppos = str.find_last_of("=", tmppos - 1);
						tmppos = str.find_last_of("<>\"'", tmppos - 1);
					}
					in_text = (tmppos != std::string::npos && str.at(tmppos) == '>');
				}

				if (cc == '<') {
					prevspecchar = curpos++;
					++tagsignlevel;
					in_nodetext = false;
					if (nc != '/' && (nc != '!' || nnc == '[')) {
						xmllevel += 2;
					}
				}
				else if (cc == '>' && !in_attribute && !in_text) {
					// > are ignored inside attributes
					if (pc != '/' && pc != ']') {
						--xmllevel;
						in_nodetext = true;
					}
					else {
						xmllevel -= 2;
					}

					if (xmllevel < 0) {
						xmllevel = 0;
					}
					--tagsignlevel;
					prevspecchar = curpos++;
				}
				else if (isEOL(cc, nc, eolmode)) {
					if (eolmode == SC_EOL_CRLF) {
						++curpos; // skipping \n of \r\n
					}

					// \n are ignored inside attributes
					nexwchar_t = str.find_first_not_of(" \t", ++curpos);

					bool skipformat = false;
					if (!autoindenttext && nexwchar_t != std::string::npos) {
						cc = str.at(nexwchar_t);
						skipformat = (cc != '<' && cc != '\r' && cc != '\n');
					}
					if (nexwchar_t >= curpos && xmllevel >= 0 && !skipformat) {
						if (nexwchar_t < 0) {
							nexwchar_t = curpos;
						}
						int delta = 0;
						str.erase(curpos, nexwchar_t - curpos);
						strlength = str.length();

						if (curpos < strlength) {
							cc = str.at(curpos);
							// we set delta = 1 if we technically can + if we are in a text or inside an attribute
							if (xmllevel > 0 && curpos < strlength - 1 && ((cc == '<' && str.at(curpos + 1) == '/') || in_attribute)) {
								delta = 1;
							}
							else if (cc == '\n' || cc == '\r') {
								delta = xmllevel;
							}
						}

						if (usetabs) {
							str.insert(curpos, (xmllevel - delta), '\t');
						}
						else {
							str.insert(curpos, tabwidth*(xmllevel - delta), ' ');
						}
						strlength = str.length();
					}
				}
				else {
					++curpos;
				}
			}
			else {
				if (in_comment && curpos > 1 && !str.compare(curpos - 2, 3, "-->")) {
					in_comment = false;
				}
				else if (in_cdata && curpos > 1 && !str.compare(curpos - 2, 3, "]]>")) {
					in_cdata = false;
				}
				else if (in_header && curpos > 0 && !str.compare(curpos - 1, 2, "?>")) {
					in_header = false;
				}
				else if (in_meta && curpos > 1 && !str.compare(curpos, 1, ">")) {
					in_meta = false;
				}
				else if (in_link && curpos > 1 && !str.compare(curpos, 1, ">")) {
					in_link = false;
				}
				++curpos;
			}
		}

		// Send formatted string to scintilla
		if (selend > selstart) {
			::SendMessage(hCurrentEditView, SCI_REPLACESEL, 0, reinterpret_cast<LPARAM>(str.c_str()));
		}
		else {
			::SendMessage(hCurrentEditView, SCI_SETTEXT, 0, reinterpret_cast<LPARAM>(str.c_str()));
		}

		str.clear();

		// Restore scrolling
		::SendMessage(hCurrentEditView, SCI_LINESCROLL, 0, yOffset);
		::SendMessage(hCurrentEditView, SCI_SETXOFFSET, xOffset, 0);
	}
}