# Pretty Print HTML or XML #

Made 2018

Mainly uses the parser for pretty printing XML in XML Tools - https://github.com/morbac/xmltools/blob/master/XMLTools.cpp

Just a simple small plugin to do this feature, without the bulk of installing XML Tools.

Also uses the Notepad++ template plugin code.

I made some slight modifications to this prettyPrint function to ignore `<meta>` and `<link>` tags in the header of an HTML file, that do not have a closing `/` character (eg `<meta />`).

Unclosed `<img>`, `<input>`, `<br>`, and `<hr>` tags will cause the program to indent.

To build the .dll - build either in x86 for 32bit Notepad++ installation, or x64 for a 64 bit installation of Notepad++.

This plugin will change text from something like this:

```xml
<note><to>Tove</to><from>Jani</from><heading>Reminder</heading><body>Don't forget me this weekend!</body></note>
```

to

```xml
<note>
	<to>Tove</to>
	<from>Jani</from>
	<heading>Reminder</heading>
	<body>Don't forget me this weekend!</body>
</note>
```


See for releasing: https://docs.gitlab.com/ce/workflow/releases.html

Also maybe upload to Notepad++ plugins
